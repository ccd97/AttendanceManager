package ccd.attendancemanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class PeriodAdapter extends RecyclerView.Adapter<PeriodAdapter.ViewHolder> {
    private List<Period> periodList;
    private Context context;

    //TODO : Add remove Functionality
    /*
    private List<Integer> selectedItems = new ArrayList<>();
    private DateCalendar calendar;
    private ActionModeCallback callback = new ActionModeCallback();
    private android.support.v7.view.ActionMode actionMode;

    */

    public PeriodAdapter(Context context, DateCalendar calendar) {
        this.context = context;
        this.periodList = new JsonParser(context).parseJson(calendar.getWeekDay());
        if (calendar.compareCalendar() == 0) removeCheckedEntries(context);
        else if (calendar.compareCalendar() == -1) periodList.clear();
    }

    private void removeCheckedEntries(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        List<Period> checkedPeriod = new ArrayList<>(periodList.size());

        Period.generateTags(periodList);

        for (Period period : periodList) {
            String totalKey = period.getTag().replaceAll("-", "_") + "_total";
            if (sp.getInt(totalKey, 0) > 0) checkedPeriod.add(period);
        }

        periodList.removeAll(checkedPeriod);
    }

    @Override
    public PeriodAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View card = LayoutInflater.from(context).inflate(R.layout.recycled_card, null);
        return new ViewHolder(card);
    }

    //TODO : Add remove Functionality
    /*
    private void toggleSelection(Integer position, View v) {

        if(selectedItems.contains(position)) selectedItems.remove(position);
        else selectedItems.add(position);

        if((boolean) v.getTag()){
            v.setBackground(new ColorDrawable(0xFFF9F9F9));
            v.setTag(false);
        } else {
            v.setBackground(new ColorDrawable(0xFFBDBDBD));
            v.setTag(true);
        }

        int count = getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
        } else {
            actionMode.setTitle(String.valueOf(count) + " selected");
            actionMode.invalidate();
        }
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }
    */

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView subjectTV;
        TextView startTimeTV;
        TextView endTimeTV;
        TextView teacherTV;
        TextView classTV;
        CardView card;

        public ViewHolder(View card) {
            super(card);
            subjectTV = (TextView) card.findViewById(R.id.subjectTV);
            startTimeTV = (TextView) card.findViewById(R.id.startTV);
            endTimeTV = (TextView) card.findViewById(R.id.endTV);
            teacherTV = (TextView) card.findViewById(R.id.teacherTV);
            classTV = (TextView) card.findViewById(R.id.classTV);
            this.card = (CardView) card.findViewById(R.id.card);

            //TODO : Add remove Functionality
            /*

            card.setTag(false);

            if(calendar.compareCalendar() == DateCalendar.EQUAL) {


                card.setOnLongClickListener(new View.OnLongClickListener() {

                    @Override
                    public boolean onLongClick(View v) {
                        MainActivity mainActivity = (MainActivity) context;
                        if (mainActivity != null) {
                            if (actionMode == null)
                                actionMode = mainActivity.startSupportActionMode(callback);
                            toggleSelection(getAdapterPosition(), v);
                            return true;
                        }
                        return false;
                    }
                });

            }

            */
        }

    }

    @Override
    public void onBindViewHolder(PeriodAdapter.ViewHolder holder, int position) {
        Period period = periodList.get(position);
        holder.subjectTV.setText(period.getSubject());
        holder.subjectTV.setTag(period.getTag());
        holder.startTimeTV.setText(period.getStartTime());
        holder.endTimeTV.setText(period.getEndTime());
        holder.teacherTV.setText(period.getTeacher());
        holder.classTV.setText(period.getClassroom());
        setAnimation(holder.card);
    }

    private void setAnimation(View viewToAnimate) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_up);
        viewToAnimate.startAnimation(animation);
    }

    @Override
    public int getItemCount() {
        return (periodList != null ? periodList.size() : 0);
    }

    public void removeItem(int position) {
        periodList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount());
    }

    //TODO : Add remove Functionality
    /*
    public class ActionModeCallback implements android.support.v7.view.ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(android.support.v7.view.ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.action_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(android.support.v7.view.ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(android.support.v7.view.ActionMode mode, MenuItem item) {
            switch(item.getItemId()){
                case R.id.menu_delete:
                    Collections.sort(selectedItems);
                    Collections.reverse(selectedItems);
                    for(int pos : selectedItems) removeItem(pos);
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(android.support.v7.view.ActionMode mode) {

        }
    }
    */
}
