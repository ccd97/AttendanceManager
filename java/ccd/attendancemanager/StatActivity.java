package ccd.attendancemanager;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ScrollView;

import java.util.List;

public class StatActivity extends AppCompatActivity {

    private Menu menu;
    private ScrollView scrollView;
    private View statView;
    private DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showAsPopup(this);
        setContentView(R.layout.activity_stat);

        scrollView = (ScrollView) findViewById(R.id.stat_scroll);

        List<String> subjectsNoRep = new JsonParser(this).getSubjectsFromJson();
        dbHelper = new DatabaseHelper(this, subjectsNoRep);

        addStatView(scrollView, true, false);

    }

    private void addStatView(ViewGroup view, boolean showPercentCol, boolean isEditable) {
        view.removeAllViews();
        DayRecord record = dbHelper.getTotalRecord();
        statView = new CardGenerator(this).getStatView(record, showPercentCol, isEditable);
        view.addView(statView);
    }

    private void showAsPopup(Activity activity) {

        activity.requestWindowFeature(Window.FEATURE_ACTION_BAR);

        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        WindowManager.LayoutParams params = activity.getWindow().getAttributes();
        params.alpha = 1.0f;
        params.dimAmount = 0.5f;

        activity.getWindow().setAttributes(params);

        Point size = new Point();

        Display display = activity.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        int width = size.x;
        int height = size.y;

        if (height > width) {
            activity.getWindow().setLayout((int) (width * .9), (int) (height * .7));
        } else {
            activity.getWindow().setLayout((int) (width * .7), (int) (height * .8));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.stat_menu, menu);

        this.menu = menu;

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_close:
                if (menu.findItem(R.id.menu_edit).isVisible()) finish();
                else {
                    menu.findItem(R.id.menu_edit).setVisible(true);
                    menu.findItem(R.id.menu_done).setVisible(false);
                    addStatView(scrollView, true, false);
                }
                break;
            case R.id.menu_edit:
                menu.findItem(R.id.menu_done).setVisible(true);
                item.setVisible(false);
                addStatView(scrollView, false, true);
                break;
            case R.id.menu_done:
                menu.findItem(R.id.menu_edit).setVisible(true);
                item.setVisible(false);
                DayRecord record = DayRecord.getRecordFromStatView(statView);
                dbHelper.updateRecord(record);
                addStatView(scrollView, true, false);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
