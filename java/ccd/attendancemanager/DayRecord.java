package ccd.attendancemanager;

import android.content.SharedPreferences;
import android.support.v4.util.Pair;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DayRecord {

    private int id;
    private HashMap<String, Pair<Integer, Integer>> record;
    private ArrayList<String> emptyRecord;

    public DayRecord(int id) {
        this.record = new HashMap<>();
        this.emptyRecord = new ArrayList<>();
        this.id = id;
    }

    public DayRecord() {
        this(-1);
    }

    public DayRecord(DateCalendar calendar) {
        this(calendar.getId());
    }

    public DayRecord(SharedPreferences preferences, List<String> subjectList) {
        this(preferences.getInt("id", 0));

        for (String subject : subjectList) {
            subject = subject.replaceAll("-", "_");
            int attended = preferences.getInt(subject + "_attended", 0);
            int total = preferences.getInt(subject + "_total", 0);

            subject = subject.replaceAll("\\d", "");

            if (record.containsKey(subject)) {
                attended += record.get(subject).first;
                total += record.get(subject).second;
            }
            addField(subject, attended, total);
        }
    }

    public DayRecord subtract(DayRecord day2) {

        DayRecord resultRecord = new DayRecord(this.id);

        HashMap<String, Pair<Integer, Integer>> record2 = day2.getRecord();

        for (String subject : record2.keySet()) {
            if (!record.containsKey(subject)) {
                Pair<Integer, Integer> pair = record2.get(subject);
                int attended = pair.first * -1;
                int total = pair.second * -1;
                resultRecord.addField(subject, attended, total);
            }
        }

        for (String subject : record.keySet()) {
            if (!record2.containsKey(subject)) {
                Pair<Integer, Integer> pair = record.get(subject);
                int attended = pair.first;
                int total = pair.second;
                resultRecord.addField(subject, attended, total);
            } else {
                Pair<Integer, Integer> pair1 = record.get(subject);
                Pair<Integer, Integer> pair2 = record2.get(subject);
                int attended = pair1.first - pair2.first;
                int total = pair1.second - pair2.second;
                resultRecord.addField(subject, attended, total);
            }
        }

        return resultRecord;

    }

    public DayRecord add(DayRecord day2) {

        DayRecord resultRecord = new DayRecord(this.id);

        HashMap<String, Pair<Integer, Integer>> record2 = day2.getRecord();

        for (String subject : record2.keySet()) {
            if (!record.containsKey(subject)) {
                Pair<Integer, Integer> pair = record2.get(subject);
                int attended = pair.first;
                int total = pair.second;
                resultRecord.addField(subject, attended, total);
            }
        }

        for (String subject : record.keySet()) {
            if (!record2.containsKey(subject)) {
                Pair<Integer, Integer> pair = record.get(subject);
                int attended = pair.first;
                int total = pair.second;
                resultRecord.addField(subject, attended, total);
            } else {
                Pair<Integer, Integer> pair1 = record.get(subject);
                Pair<Integer, Integer> pair2 = record2.get(subject);
                int attended = pair1.first + pair2.first;
                int total = pair1.second + pair2.second;
                resultRecord.addField(subject, attended, total);
            }
        }

        return resultRecord;

    }

    public void addField(String key, int attended, int total) {
        key = key.replaceAll("_", "-");
        if (attended != 0 || total != 0)
            record.put(key, new Pair<>(attended, total));
        else emptyRecord.add(key);
    }

    public static DayRecord getRecordFromStatView(View view) {

        DayRecord record = new DayRecord();

        LinearLayout subjectLL = (LinearLayout) view.findViewById(R.id.stat_subject);
        LinearLayout attendedLL = (LinearLayout) view.findViewById(R.id.stat_attended);
        LinearLayout totalLL = (LinearLayout) view.findViewById(R.id.stat_total);

        if (subjectLL == null || attendedLL == null || totalLL == null) return record;

        int count = totalLL.getChildCount();

        for (int i = 2; i < count; i += 2) {
            TextView subjectTV = (TextView) subjectLL.getChildAt(i);
            TextView attendedTV = (TextView) attendedLL.getChildAt(i);
            TextView totalTV = (TextView) totalLL.getChildAt(i);

            String subject = String.valueOf(subjectTV.getText());
            int attended = Integer.parseInt(String.valueOf(attendedTV.getText()));
            int total = Integer.parseInt(String.valueOf(totalTV.getText()));

            record.addField(subject, attended, total);
        }

        return record;

    }

    public HashMap<String, Pair<Integer, Integer>> getRecord() {
        return record;
    }

    public ArrayList<String> getEmptyRecord() {
        return emptyRecord;
    }

    public int getId() {
        return id;
    }
}
