package ccd.attendancemanager;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

class CardGenerator {

    private Context context;
    private LinearLayout.LayoutParams textParams, divParams;

    public CardGenerator(Context context) {
        this.context = context;
        divParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1);
        textParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        textParams.setMargins(7, 7, 7, 7);
    }

    private View getNoDataCard() {
        return getMessageCard(context.getResources().getString(R.string.no_data));
    }

    public View getHolidayCard() {
        return getMessageCard(context.getResources().getString(R.string.holiday));
    }

    private View getMessageCard(String message) {

        View inflatedView = LayoutInflater.from(context).inflate(R.layout.message_card, null);

        TextView messageTV = (TextView) inflatedView.findViewById(R.id.message_tv);
        messageTV.setText(message);

        CardView card = (CardView) inflatedView.findViewById(R.id.main_message_card);
        CardView.LayoutParams cardParams = new CardView
                .LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        cardParams.setMargins(12, 0, 12, 15);
        card.setLayoutParams(cardParams);
        card.setCardElevation(5);
        card.setRadius(6);

        return inflatedView;
    }

    public View getStatView(DayRecord record, boolean showPercentCol, boolean isEditable) {

        HashMap<String, Pair<Integer, Integer>> statMap = record.getRecord();

        if (statMap == null || statMap.isEmpty()) return getNoDataCard();

        View inflatedView = LayoutInflater.from(context).inflate(R.layout.status_card, null);

        LinearLayout subjectLL = (LinearLayout) inflatedView.findViewById(R.id.stat_subject);
        LinearLayout attendedLL = (LinearLayout) inflatedView.findViewById(R.id.stat_attended);
        LinearLayout totalLL = (LinearLayout) inflatedView.findViewById(R.id.stat_total);
        LinearLayout percentLL = (LinearLayout) inflatedView.findViewById(R.id.stat_percent);

        if (!showPercentCol) {
            View percentDiv = inflatedView.findViewById(R.id.stat_percent_div);
            percentDiv.setVisibility(View.GONE);
            percentLL.setVisibility(View.GONE);
        }

        for (HashMap.Entry<String, Pair<Integer, Integer>> entry : statMap.entrySet()) {
            String subject = entry.getKey();
            int attended = entry.getValue().first;
            int total = entry.getValue().second;

            addIndividualStat(subjectLL, attendedLL, totalLL, percentLL,
                    subject, attended, total, isEditable, showPercentCol);

        }

        CardView card = (CardView) inflatedView.findViewById(R.id.stat_card);
        CardView.LayoutParams cardParams = new CardView
                .LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        cardParams.setMargins(12, 0, 12, 15);
        card.setLayoutParams(cardParams);
        card.setCardElevation(5);
        card.setRadius(6);

        ImageButton imageButton = (ImageButton) inflatedView.findViewById(R.id.arrow_button);
        setImageButton(imageButton, record.getEmptyRecord(), isEditable, showPercentCol);

        if(!isEditable && !showPercentCol){
            card.setTag(true);
            card.setOnLongClickListener(statCardListener());
        }

        return inflatedView;
    }

    //TODO : Add option to change elements
    private CardView.OnLongClickListener statCardListener(){
        return new CardView.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                if((boolean) v.getTag()) {
                    v.setBackground(new ColorDrawable(0xFFBDBDBD));
                    v.setTag(false);
                }
                else {
                    v.setBackground(new ColorDrawable(0xFFF9F9F9));
                    v.setTag(true);
                }
                return true;
            }
        };
    }

    private void addStatField(LinearLayout parent, TextView child, String text, int alignment){
        child.setLayoutParams(textParams);
        child.setTextAlignment(alignment);
        child.setText(text);

        parent.addView(child);
        parent.addView(getDivider());
    }

    private View getDivider(){
        ImageView divider = new ImageView(context);
        divider.setLayoutParams(divParams);
        divider.setBackgroundColor(Color.GRAY);
        return divider;
    }

    private void addIndividualStat(LinearLayout subjectLL, LinearLayout attendedLL,
                                   LinearLayout totalLL, LinearLayout percentLL,
                                   String subject, int attended, int total,
                                   boolean isEditable, boolean showPercentCol){

        final TextView subjectTV, attendedTV, totalTV;
        subjectTV = new TextView(context);

        if (isEditable) {
            attendedTV = new EditText(context);
            totalTV = new EditText(context);
            attendedTV.setInputType(InputType.TYPE_CLASS_NUMBER);
            totalTV.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else {
            attendedTV = new TextView(context);
            totalTV = new TextView(context);
        }

        addStatField(subjectLL, subjectTV, subject, View.TEXT_ALIGNMENT_TEXT_START);

        addStatField(attendedLL, attendedTV, String.valueOf(attended), View.TEXT_ALIGNMENT_CENTER);

        addStatField(totalLL, totalTV, String.valueOf(total), View.TEXT_ALIGNMENT_CENTER);

        subjectTV.getViewTreeObserver().addOnGlobalLayoutListener
                (new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        LinearLayout.LayoutParams subParams = new LinearLayout
                                .LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                totalTV.getHeight());
                        subParams.setMargins(7, 7, 7, 7);
                        subjectTV.setLayoutParams(subParams);
                        subjectTV.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }

                });

        if (showPercentCol) {
            TextView percentTv = new TextView(context);

            float percent = 100f * attended / total;

            addStatField(percentLL, percentTv,
                    String.format(Locale.US, "%.2f", percent), View.TEXT_ALIGNMENT_CENTER);
        }

    }

    private void setImageButton(ImageButton imageButton, final ArrayList<String> emptyRecord,
                                final boolean isEditable, final boolean showPercentCol) {

        imageButton.setTag(true);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View baseView = (View) v.getParent().getParent();
                LinearLayout subjectLL = (LinearLayout) baseView.findViewById(R.id.stat_subject);
                LinearLayout attendedLL = (LinearLayout) baseView.findViewById(R.id.stat_attended);
                LinearLayout totalLL = (LinearLayout) baseView.findViewById(R.id.stat_total);
                LinearLayout percentLL = (LinearLayout) baseView.findViewById(R.id.stat_percent);

                if ((boolean) v.getTag()){
                    v.setTag(false);
                    v.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_expand_less_black_24dp));

                    for(String subject : emptyRecord){
                        addIndividualStat(subjectLL, attendedLL, totalLL, percentLL, subject,
                                0, 0, isEditable, showPercentCol);
                    }
                }
                else {
                    v.setTag(true);
                    v.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_expand_more_black_24dp));

                    for (int i=2; i<subjectLL.getChildCount(); i+=2){
                        TextView tmpTV = (TextView) totalLL.getChildAt(i);
                        if (tmpTV.getText() == String.valueOf(0)){
                            subjectLL.removeViews(i, emptyRecord.size()*2);
                            attendedLL.removeViews(i, emptyRecord.size()*2);
                            totalLL.removeViews(i, emptyRecord.size()*2);
                            if (showPercentCol) percentLL.removeViews(i, emptyRecord.size()*2);
                            break;
                        }
                    }
                }
            }
        });
    }
}
