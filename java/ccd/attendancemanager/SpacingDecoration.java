package ccd.attendancemanager;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

class SpacingDecoration extends RecyclerView.ItemDecoration {

    private int spacing;

    public SpacingDecoration(int spacing) {
        this.spacing = spacing;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        outRect.bottom = spacing;
        outRect.top = spacing;
        outRect.left = spacing;
        outRect.right = spacing;

    }

}
