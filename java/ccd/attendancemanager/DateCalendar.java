package ccd.attendancemanager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

class DateCalendar {

    public static final int GREATER = 1;
    public static final int SMALLER = -1;
    public static final int EQUAL = 0;

    public static final int SUNDAY = -2;

    private Calendar calendar;
    private int id;

    public DateCalendar(int year, int month, int date) {
        this.calendar = Calendar.getInstance();
        calendar.set(year, month, date);
        this.id = generateId();
    }

    public DateCalendar() {
        this.calendar = Calendar.getInstance();
        this.id = generateId();
    }

    public int getId() {
        return id;
    }

    private int generateId() {
        int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
        int year = calendar.get(Calendar.YEAR);
        return (int) ((year - 2000) * 365.2425 + dayOfYear);
    }

    public int compareCalendar() {
        return compareCalendar(new DateCalendar());
    }

    private int compareCalendar(DateCalendar cal) {
        int id1 = cal.getId();
        if (this.calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
            return SUNDAY;
        else return (id > id1) ? GREATER : (id < id1) ? SMALLER : EQUAL;
    }

    public String getWeekDay() {
        return new SimpleDateFormat("EEEE", Locale.US).format(calendar.getTime());
    }

    public String getDateString() {
        return new SimpleDateFormat("dd MMMM", Locale.US).format(calendar.getTime());
    }

    public Date getTime() {
        return calendar.getTime();
    }
}
